package hashcode2015;

import java.util.Comparator;

public class ServerComparatorDescending  implements Comparator<Server>
{
	@Override
	public int compare(Server server1, Server server2)
	{
		//ascending
		//return this.density - server.density;
		
		//descending
		return (int)(100*(server2.density - server1.density));
	}
}
